<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('middle_name');
            $table->dropColumn('age');
            $table->dropColumn('address');
            $table->dropColumn('birth_day');
            $table->dropColumn('phone_number');
            $table->dropColumn('has_hereditic_dissease');
            $table->dropColumn('hereditic_dissease');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->string('middle_name')->nullable();
            $table->integer('age')->nullable();
            $table->text('address')->nullable();
            $table->date('birth_day')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('has_hereditic_dissease')->nullable();
            $table->text('hereditic_dissease')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

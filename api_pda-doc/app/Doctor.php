<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table = 'doctors_info';

    protected $fillable = [
    	'doctors_id',
    	'approved',
    	'doctors_category',
    	'educational_attainment'
    ];

    protected $visible = [
    	'doctors_id',
    	'approved',
    	'doctors_category',
    	'educational_attainment'
    ];
}

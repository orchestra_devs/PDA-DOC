<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendations extends Model
{
    protected $table = 'messages';

    protected $fillable = [
    	'messages',
    	'reciever',
    	'sender'
    ];

    protected $visible = [
    	'messages',
    	'reciever',
    	'sender'
    ];
}

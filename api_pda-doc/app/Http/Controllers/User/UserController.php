<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Doctor;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $users)
    {
        $user = $users->all();

        return $this->setStatus(200)
                    ->setMessage('success')
                    ->respond($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $users, Doctor $doctor)
    {
        $user = $users->create(
                    [
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'middle_name' => $request->middle_name,
                        'username' => $request->username,
                        'age' => $request->age,
                        'address' => $request->address,
                        'birth_day' => $request->birth_day,
                        'email_address' => $request->email_address,
                        'password' => bcrypt($request->password),
                        'type' => $request->type, 
                    ]
                );

        if ($request->type === 'doctor') {
            $doctor = $doctor->create(
                    [
                        'doctors_id' => $user->id,
                        'approved' => 1
                    ]
                );
        }

        return $this->setStatus(200)
                    ->setMessage('saved')
                    ->respond($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, User $users)
    {
        $user = $users->findOrFail($id);

        return $this->setStatus(200)
                ->setMessage('success')
                ->respond($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, User $users)
    {
        $user = $users->where('id', $id)
                      ->update([
                            $request->all()
                        ]);

        return $this->setStatus(200)
                    ->setMessage('updated')
                    ->respond($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, User $users)
    {
        $user = $users->destroy($id);

        return $this->setStatus(200)
                    ->setMessage('deleted')
                    ->respond($user);
    }
}

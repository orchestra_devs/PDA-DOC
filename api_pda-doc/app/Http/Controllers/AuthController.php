<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function postLogin(Request $request)
    {	
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
        	return $this->setStatus(200)
        				->setMessage('welcome')
        				->respond(Auth::user());
        }

        return $this->setStatus(401)
        				->setMessage('not authorize')
        				->respond();
    }

    public function getLogout()
    {
        return Auth::logout();
    }
}

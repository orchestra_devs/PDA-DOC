<?php
namespace App\Http\Common;
/**
 * @usage
 *
 * $this
 *		->setStatus(400)
 * 		->setMessage('MESSAGE HERE')
 *		->respond(['data' => $data]);
 *
 */
trait ApiTrait
{
	/**
	 * Response message
	 * @var str
	 */
	protected $message = 'Successfully fetched data';
	/**
	 * Response HTTP code
	 * @var int
	 */
	protected $status = 200;
	/**
	 * Response headers
	 * @var array
	 */
	protected $headers = [];
	/**
	 * Sets a header
	 * 
	 * @param string $key
	 * @param mixed $value
	 */
	protected function addHeader($key, $value)
	{
		$this->headers[$key] = $value;
		return $this;
	}
	/**
	 * Sets a status
	 * 
	 * @param mixed $status
	 */
	protected function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}
	/**
	 * Sets a message
	 * 
	 * @param string $message
	 */
	protected function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}
	/**
	 * method of adding multiple headers
	 *
	 * @param headers
	 * @return Response
	 */
	protected function addHeaders(array $headers)
	{
		$meta = [
			'status'  => $this->status,
			'message' => $this->message
		];
		return response()->json(
				$meta,
				$this->status,
				$this->headers
			);
	}
    /**
     * Base respond-method.
     *
     * If you need to create a new custom response for the trait
     * or for the consuming class, use this one!
     *
     * @param {mixed} $data
     * @return Response
     */
    protected function respond($data = null)
    {
        $meta = [
            'status'    => $this->status,
            'message'   => $this->message
        ];

        if(! is_null($data)) {
            $meta['data'] = $data;
        }

        return response()->json(
            $meta,
            $this->status,
            $this->headers
        );
    }
	/**
	 * @param array $data
	 * @return Response
	 */
	protected function respondCreated($data = [])
	{
		$meta = [
			'status'  => 201,
			'message' => $this->message,
			'data'    => $data
		];
		return response()->json(
				$meta,
				201,
				$this->headers
			);
	}
	/**
	 * method for responding with data
	 *
	 * @param array $data
	 * @return Response
	 */
	public function respondWithData(array $data)
	{
		return $this->respond($data);
	}
	/**
	 * method for returning a status 404
	 * @return respond
	 */
	public function respondNotFound($data = [])
	{
		$meta = [
			'status'  => 404,
			'message' => 'page not found',
			'data'    => $data
		];
		return respond([
				$meta,
				$this->status,
				$this->headers
			]);
	}
	/**
	 * method for returning a status 500
	 * @return Response
	 */
	public function respondInternalError($data = [])
	{
		$meta = [
			'status'  => 500,
			'message' => 'internal server error',
			'data'    => $data
		];
		return respond([
				$meta,
				$this->status,
				$this->headers
			]);
	}
}
<?php

Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'User\UserController');
    Route::resource('recommendations', 'User\RecommendationsController');
    Route::resource('doctors', 'DoctorController');
});

Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@getLogout');
$(function() {
    /**
     * [oData description]
     * @type {Array}
     */
    var oData = [];

    //Add Volunteer
    $("#btnVolunteer").click(function(){
        oData = {
            'first_name' : $("#txtFname").val(),
            'last_name'  : $("#txtLname").val(),
            'username'   : $("#txtUsername").val(),
            'password'   : $("#txtPassword").val(),
            'type'       : 'doctor',
            'email'      : $("#txtEmail").val()
        };

        $.ajax({
            type: 'POST',
            url: '/users/',
            data: oData,
            encode: 'json',
            success:function(aResponse){
                window.location.href = "http://stackoverflow.com/success.html";
            },
            error:function(aResponse){
                console.log('ERRORS: Sign up volunteer');
                console.log(aResponse);
            }
        });
    });

    $("#btnSignIn").click(function(){
        oData = {
            'username'   : $("#txtUsername").val(),
            'password'   : $("#txtPassword").val(),
        };

        $.ajax({
            type: 'POST',
            url: '/login/',
            data: oData,
            encode: 'json',
            success:function(aResponse){
                if (aResponse.Data.type === 'doctor') {
                    window.location.href = "http://stackoverflow.com/admin/dashboard.html";
                } else {
                    window.location.href = "http://stackoverflow.com/admin/doctors.html";
                }
            },
            error:function(aResponse){
                console.log('ERRORS: Sign up volunteer');
                console.log(aResponse);
            }
        });
    });

        //Add Volunteer
    $("#btnSaveVolunteer").click(function(){
        oData = {
            'first_name' : $("#txtFname").val(),
            'last_name'  : $("#txtLname").val(),
            'username'   : $("#txtUsername").val(),
            'password'   : $("#txtPassword").val(),
            'type'       : 'doctor',
            'email'      : $("#txtEmail").val()
        };

        $.ajax({
            type: 'POST',
            url: '/users/',
            data: oData,
            encode: 'json',
            success:function(aResponse){
                window.location.href = "http://stackoverflow.com/admin/doctors.html";
            },
            error:function(aResponse){
                console.log('Add Volunteer');
                console.log(aResponse);
            }
        });
    });

    $("#secDoctors").show(function(){
        $.ajax({
            type: 'GET',
            url: '/doctors/',
            data: oData,
            encode: 'json',
            success:function(aResponse){
               $(aResponse.Data).each(function(sKey, sValue){
                    $("#listDoctors").append("<div class='col-sm-3 col-xs-12 col-md-3 col-lg-3 text-center'> <i class='fa fa-user-md fa-5x'></i> <h4><a href=' data-id='>"+ sValue.first_name + " " + sValue.last_name +"</a></h4> <h4><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'><i class='fa fa-star-o'></i></i></h4> <a href=' data-id='" + sValue.id + "'class='btn btn-lg btn-shell-icon'><i class='fa fa-search'></i></a> <a  href=' data-id='" + sValue.id + "'class='btn btn-lg btn-shell-icon'><i class='fa fa-edit'></i></a> <a href=' data-id='" + sValue.id + "'class='btn btn-lg btn-shell-icon'><i class='fa fa-trash'></i></a> </div>");
               }); 
            },
            error:function(aResponse){
                console.log('ERRORS: Listing doctors');
                console.log(aResponse);
            }
        });
    });

});

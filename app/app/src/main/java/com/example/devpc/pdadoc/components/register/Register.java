package com.example.devpc.pdadoc.components.register;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.devpc.pdadoc.R;
import com.example.devpc.pdadoc.components.domain.model.Patient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Register extends AppCompatActivity {
    Context context = this;
    Calendar mCalendar ;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog dateDialog;

    @BindView(R.id.et_fname) EditText et_fname;
    @BindView(R.id.et_lname) EditText et_lname;
    @BindView(R.id.et_address) EditText et_address;
    @BindView(R.id.et_email) EditText et_email;
    @BindView(R.id.et_phone_num) EditText et_phone_num;
    @BindView(R.id.et_bday) EditText et_bday;
    @BindView(R.id.spn_age) Spinner spn_age;
    @BindView(R.id.btn_save) Button btn_save;
    @BindView(R.id.et_pass) EditText et_pass;
    @BindView(R.id.et_confirm) EditText et_confirm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("MM-dd-yyyy");

        initComponents();



    }

    @OnClick(R.id.btn_save) void clickReg(){

        et_fname.setText("edgar");
        et_lname.setText("pelonio");
        et_bday.setText("08-20-2016");
        et_address.setText("malolos,bulacan");
        et_email.setText("edgar@mail.com");
        et_phone_num.setText("09066840869");
        et_pass.setText("edgar");

        String sFname =  et_fname.getText().toString();
        String sLname = et_lname.getText().toString();
        int iAge = Integer.parseInt(spn_age.getSelectedItem().toString());
        String sBday = et_bday.getText().toString();
        String sEmail = et_email.getText().toString();
        String sPass = et_pass.getText().toString();
        String sConfirmPass = et_confirm.getText().toString();
        String sNum = et_phone_num.getText().toString();



        if(isValidReg(sFname,sLname,iAge,sBday,sEmail,sPass,sPass,sConfirmPass,sNum ) == true){
            Patient oPatient = new Patient(
                    et_fname.getText().toString(),et_lname.getText().toString(),Integer.parseInt(spn_age.getSelectedItem().toString()),
                    et_bday.getText().toString(),et_address.getText().toString(),et_email.getText().toString(),et_confirm.getText().toString(),et_phone_num.getText().toString()
            );
        }




    }

    @OnClick(R.id.et_bday) void getDate(){
        dateDialog.show();
    }




    private boolean isValidReg(String fname,String lname,int age ,String bday , String address,String email ,String pass ,String confirmPass, String phoneNum)
    {
        boolean bResult = true;

        if(fname.isEmpty()){
            Toast.makeText(context,"Missing first name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(lname.isEmpty()){
            Toast.makeText(context,"Missing last name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(bday.isEmpty()){
            Toast.makeText(context,"Missing birthday name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(address.isEmpty()){
            Toast.makeText(context,"Missing address name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(email.isEmpty()){
            Toast.makeText(context,"Missing email name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(pass.isEmpty()){
            Toast.makeText(context,"Missing password name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }
        else if(phoneNum.isEmpty()){
            Toast.makeText(context,"Missing password name",Toast.LENGTH_SHORT).show();
            bResult = false;
        }

        return bResult;

    }



    private void initComponents()
    {

        /////////spinner age

        ArrayList<String> aAge = new ArrayList<>();
        for (int x=1 ; x<= 100; x++) {
            aAge.add(String.valueOf(x));
        }
        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,aAge);
        spn_age.setAdapter(adapterAge);

        ////////////////date picker
        dateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                //fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                et_bday.setText(dateFormatter.format(newDate.getTime()));
            }

        },mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

    }






}

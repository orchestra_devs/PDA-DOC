package com.example.devpc.pdadoc.components.domain.api;

import com.example.devpc.pdadoc.components.domain.model.Patient;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by devpc on 8/20/2016.
 */
public interface PdaDoc {

    @GET("registerUser.php")
    void getUser(
            Callback<Patient> cb
    );

}

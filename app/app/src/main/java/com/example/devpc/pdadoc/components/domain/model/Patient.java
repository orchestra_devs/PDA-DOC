package com.example.devpc.pdadoc.components.domain.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by devpc on 8/20/2016.
 */
public class Patient implements Serializable{
    private String sFname;
    private String sLname;
    private int iAge;
    private String dateBday;
    private String sAddress;
    private String sEmail;
    private String sPass;
    private String sPhonenum;

    public Patient(String sFname, String sLname, int iAge, String dateBday, String sAddress, String sEmail, String sPass, String sPhonenum) {
        this.sFname = sFname;
        this.sLname = sLname;
        this.iAge = iAge;
        this.dateBday = dateBday;
        this.sAddress = sAddress;
        this.sEmail = sEmail;
        this.sPass = sPass;
        this.sPhonenum = sPhonenum;
    }

    public String getsFname() {
        return sFname;
    }

    public void setsFname(String sFname) {
        this.sFname = sFname;
    }

    public String getsLname() {
        return sLname;
    }

    public void setsLname(String sLname) {
        this.sLname = sLname;
    }

    public int getiAge() {
        return iAge;
    }

    public void setiAge(int iAge) {
        this.iAge = iAge;
    }

    public String getDateBday() {
        return dateBday;
    }

    public void setDateBday(String dateBday) {
        this.dateBday = dateBday;
    }

    public String getsAddress() {
        return sAddress;
    }

    public void setsAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getsPass() {
        return sPass;
    }

    public void setsPass(String sPass) {
        this.sPass = sPass;
    }

    public String getsPhonenum() {
        return sPhonenum;
    }

    public void setsPhonenum(String sPhonenum) {
        this.sPhonenum = sPhonenum;
    }
}

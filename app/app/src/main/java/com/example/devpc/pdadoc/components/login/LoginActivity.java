package com.example.devpc.pdadoc.components.login;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.devpc.pdadoc.R;
import com.example.devpc.pdadoc.components.dashboard.fragments.Dashboard;
import com.example.devpc.pdadoc.components.register.Register;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity{

    Context context = this;
    // UI references.

    @BindView(R.id.et_username) EditText et_username;
    @BindView(R.id.et_pass) EditText et_pass;
    @BindView(R.id.btn_log) Button btn_log;
    @BindView(R.id.btn_reg) Button btn_reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Intent intent = new Intent(context,Dashboard.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @OnClick(R.id.btn_log) void login(){
        Toast.makeText(context,"sample",Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_reg) void register(){

        Toast.makeText(context,"pasok",Toast.LENGTH_LONG).show();
        Intent intentReg = new Intent(context, Register.class);
        startActivity(intentReg);
    }






    public boolean isUsernameValid(String sUsername)
    {

        return true ;
    }

    private boolean isValidPass(String sPass)
    {

        return true ;
    }

}

